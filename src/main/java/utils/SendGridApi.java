package utils;


import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import model.users.User;

import java.io.IOException;


/**
 * class handles connection with sendgrid
 * create templates on sendgrid and
 * call them here using the
 * template ID
 * ensure method name matches template name
 */

public class SendGridApi {
    private String baseUrl = "https://api.sendgrid.com/v3/";
    private String authToken = "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";

    public void sendVerificationEmail(User user) throws IOException {

        String data = String.format("{\n" +
                "    \"from\": {\"email\": \"a00290543@student.ait.ie\"},\n" +
                "    \"personalizations\": [\n" +
                "        {\n" +
                "            \"to\": [{\"email\": \"%s\"}],\n" +
                "        }\n" +
                "    ],\n" +
                "    \"template_id\": \"d-2286970fd6f840969087bbc5fb02312f\",\n" +
                "\"subject\":\"Email Verification\"" +
                "}", user.getEmail());


        String uri = baseUrl + "mail/send";
        HttpResponse<JsonNode> response = Unirest.post(uri)
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + authToken)
                .body(data).asJson();

    }
}


